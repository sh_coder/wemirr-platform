package com.wemirr.platform.authority.service;


import com.wemirr.framework.boot.service.SuperService;
import com.wemirr.platform.authority.domain.entity.common.Dictionary;

/**
 * <p>
 * 业务接口
 * 字典类型
 * </p>
 *
 * @author Levin
 * @date 2019-07-02
 */
public interface DictionaryService extends SuperService<Dictionary> {

    void addDictionary(Dictionary dictionary);

    void deleteById(Long id);

    void editDictionary(Dictionary dictionary);
}
