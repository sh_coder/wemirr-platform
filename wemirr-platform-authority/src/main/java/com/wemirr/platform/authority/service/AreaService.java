package com.wemirr.platform.authority.service;

import com.wemirr.framework.boot.service.SuperService;
import com.wemirr.platform.authority.domain.entity.common.AreaEntity;

import java.util.List;

/**
 * @author Levin
 */
public interface AreaService extends SuperService<AreaEntity> {


    List<AreaEntity> listArea(Integer parentId);

}
